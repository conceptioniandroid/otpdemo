package com.conceptioni.otpdemo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    OtpView otpView;
    TextViewRegular timer, resend;
    TextViewLight timertext;
    ProgressBar pb;
    String TAG = "OtpActivity";
    String number = "";
    String OTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        otpView = findViewById(R.id.otp_view);
        timer = findViewById(R.id.timer);
        timertext = findViewById(R.id.timertext);
        resend = findViewById(R.id.resend);
        pb = findViewById(R.id.pb);

        number = getIntent().getStringExtra("number");

        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                OTP = otp;
            }
        });

        findViewById(R.id.btn_verify).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Map<String, String> params = new HashMap<>();
                params.put("API", "verify_otp_api");
                params.put("phone", number);
                params.put("otp", OTP);*/
               // verifyOtp(params);
            }
        });

        resend.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                timer.setText("");
                timertext.setText("Your OTP will expired in");
                timer.setTextSize(16);

                startTimer();

                Map<String, String> params = new HashMap<>();
                params.put("API", "send_otp_api");
                params.put("phone", number);

                Log.d(TAG, "onClick: " + params.toString());

                //sendOtp(params);
            }
        });

        Map<String, String> params = new HashMap<>();
        params.put("API", "send_otp_api");
        params.put("phone", number);

        Log.d(TAG, "onClick: " + params.toString());

      //  sendOtp(params);
        startTimer();

    }


    private void startTimer() {
        new CountDownTimer(300000, 1000) { // adjust the milli seconds here

            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            public void onTick(long millisUntilFinished) {
                timer.setText("" + String.format("(%d min : %d sec)",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            @SuppressLint("SetTextI18n")
            public void onFinish() {
                timertext.setText("Your OTP is expired.");
                timer.setText("Click on Resend Button in order to receive another OTP.");
                timer.setTextSize(12);
            }
        }.start();
    }

  /*  public void sendOtp(final Map<String, String> params) {

        pb.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.send_otp_api, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    Toast.makeText(OtpActivity.this, "OTP sent", Toast.LENGTH_SHORT).show();
                }

                pb.setVisibility(View.GONE);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    public void verifyOtp(final Map<String, String> params) {

        pb.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(getApplication());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.verify_otp_api, new Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        JSONObject object = new JSONObject(response);

                        if (object.getString("success").equalsIgnoreCase("1")) {
                            Toast.makeText(OtpActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent in = new Intent(OtpActivity.this, LoginActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(in);
                        } else {
                            Toast.makeText(OtpActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                pb.setVisibility(View.GONE);

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }*/
}
