package com.conceptioni.otpdemo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by 123 on 21-02-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextViewSemiThin extends TextView {

    public TextViewSemiThin(Context context) {
        super(context);
        init();
    }

    public TextViewSemiThin(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewSemiThin(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/POPPINS-THIN.TTF");
        setTypeface(tf);
    }
}
